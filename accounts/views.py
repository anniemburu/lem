from django.shortcuts import render
from .forms import AccountsForm


# Create your views here.
def user_accounts(request):
    if request.method == 'POST':
        form = AccountsForm(request.POST)

        if form.is_valid():
            pass

    else:
        form = AccountsForm()

    return render(request, 'accounts/accounts.html', {'form': form})


