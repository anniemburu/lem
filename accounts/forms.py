from django import forms

from accounts.models import Accounts


class AccountsForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Accounts
        fields = ['name', 'email', 'mobile', 'password']

